package com.cellbrokerage.werkzoeken.util

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import java.text.SimpleDateFormat
import java.util.*

object Util {

    const val DATE_FORMAT = "yyyy-MM-dd"

    fun getTodayString(): String {
        return SimpleDateFormat(DATE_FORMAT, Locale.getDefault()).format(Date(System.currentTimeMillis()))
    }

    fun verifyAvailableNetwork(activity: Activity): Boolean {
        val connectivityManager = activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

}