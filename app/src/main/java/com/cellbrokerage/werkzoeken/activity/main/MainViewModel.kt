package com.cellbrokerage.werkzoeken.activity.main

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cellbrokerage.werkzoeken.BuildConfig
import com.cellbrokerage.werkzoeken.restapi.RetrofitClient
import com.cellbrokerage.werkzoeken.restapi.shift.ShiftResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class MainViewModel : ViewModel() {

    private val parentJob = Job()

    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Default

    private val scope = CoroutineScope(coroutineContext)

    val shiftsLiveData: MutableLiveData<MutableList<com.cellbrokerage.werkzoeken.model.Job>> = MutableLiveData()
    val shiftProgressBarVisibilityLiveData :MutableLiveData<Boolean> = MutableLiveData()
    val shiftErrorLiveData: MutableLiveData<Boolean> = MutableLiveData()

    fun fetchShifts() {
        scope.launch {
            shiftProgressBarVisibilityLiveData.postValue(true)
            try {
                val response = RetrofitClient.serviceApi.getShifts().execute()

                if (response.isSuccessful) {
                    val body = response.body()

                    if (body != null) {
                        shiftsLiveData.postValue(convertToList(body))
                    } else {
                        shiftErrorLiveData.postValue(true)
                    }
                } else {
                    shiftErrorLiveData.postValue(true)

                    if (BuildConfig.DEBUG)
                        Log.e(
                            "MainViewModel",
                            "Code: " + response.code() + "\nError: " + response.errorBody()?.string()
                        )
                }
            } catch (e: Exception) {
                shiftErrorLiveData.postValue(true)

                if (BuildConfig.DEBUG)
                    e.printStackTrace()
            }
            shiftProgressBarVisibilityLiveData.postValue(false)
        }
    }

    private fun convertToList(body: ShiftResponse): MutableList<com.cellbrokerage.werkzoeken.model.Job> {
        val resultList: MutableList<com.cellbrokerage.werkzoeken.model.Job> = mutableListOf()

        val dataMap = body.data

        for (dataObject in dataMap) {
            val jobList = dataMap[dataObject.key]

            if (jobList != null) {
                for (job in jobList) {
                    resultList.add(job)
                }
            }
        }

        return resultList
    }

}
