package com.cellbrokerage.werkzoeken.activity.main

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.cellbrokerage.werkzoeken.R
import com.cellbrokerage.werkzoeken.activity.main.adapter.ShiftRecyclerAdapter
import com.cellbrokerage.werkzoeken.util.Util
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel
    private lateinit var shiftRecyclerAdapter: ShiftRecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        viewModel.shiftsLiveData.observe(this, Observer {
            shiftRecyclerAdapter.updateItems(it)
        })
        viewModel.shiftProgressBarVisibilityLiveData.observe(this, Observer {
            progressBar.visibility = if (it) View.VISIBLE else View.GONE
        })
        viewModel.shiftErrorLiveData.observe(this, Observer {
            Snackbar.make(activity_main, R.string.error_unknown_error, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.action_retry) { attemptFetchShifts() }.show()
        })

        if (viewModel.shiftsLiveData.value == null) {
            attemptFetchShifts()
        }

        shiftRecyclerAdapter = ShiftRecyclerAdapter(null)

        with(shiftsRecyclerView) {
            layoutManager = LinearLayoutManager(this@MainActivity)

            adapter = shiftRecyclerAdapter
        }
    }

    private fun attemptFetchShifts() {
        if (Util.verifyAvailableNetwork(this@MainActivity)) {
            viewModel.fetchShifts()
        } else {
            Snackbar.make(activity_main, R.string.error_unknown_error, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.action_retry) { viewModel.fetchShifts() }.show()
        }
    }

}
