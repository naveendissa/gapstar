package com.cellbrokerage.werkzoeken.activity.main.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.cellbrokerage.werkzoeken.R
import com.cellbrokerage.werkzoeken.model.Job
import com.cellbrokerage.werkzoeken.model.Shift
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_job_list.view.*
import java.lang.StringBuilder

class ShiftRecyclerAdapter(private val listener: OnListItemClickListener?) :
    RecyclerView.Adapter<ShiftRecyclerAdapter.ViewHolder>() {

    private var items: MutableList<Job> = mutableListOf()

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Job

            listener?.onItemClick(item)
        }
    }

    interface OnListItemClickListener {
        fun onItemClick(item: Job)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_job_list, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val job = items[position]

        Picasso.get().load(job.client.photos[0].formats[0].cdn_url).into(holder.clientImageView)

        holder.titleTextView.text = job.title
        holder.timeTextView.text = getTime(job.shifts)
        holder.priceTextView.text = getEarnings(job.shifts)
        holder.categoryDistanceTextView.text = job.job_category.description + " - " + job.distance
    }

    private fun getTime(shifts: MutableList<Shift>): CharSequence {
        val builder = StringBuilder()
        for (shift in shifts) {
            builder.append(shift.start_time)
            builder.append(" - ")
            builder.append(shift.end_time)
            builder.append(", ")
        }

        builder.setLength(builder.length - 2)

        return builder.toString()
    }

    private fun getEarnings(shifts: MutableList<Shift>): CharSequence {
        var total = 0.0
        for (shift in shifts) {
            total += shift.earnings_per_hour
        }

        return shifts[0].currency + " " + total
    }

    fun updateItems(newItems: MutableList<Job>) {
        items = newItems
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val titleTextView: TextView = itemView.titleTextView
        val timeTextView: TextView = itemView.timeTextView
        val clientImageView: ImageView = itemView.clientImageView
        val priceTextView: TextView = itemView.priceTextView
        val categoryDistanceTextView: TextView = itemView.categoryDistanceTextView
    }

}
