package com.cellbrokerage.werkzoeken.restapi

import com.cellbrokerage.werkzoeken.AppConstants
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitClient {

    private val retrofitClient: Retrofit = Retrofit.Builder()
        .baseUrl(AppConstants.API_BASE_URL)
        .client(
            OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build()
        )
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val serviceApi: ServiceApi = retrofitClient.create(ServiceApi::class.java)

}