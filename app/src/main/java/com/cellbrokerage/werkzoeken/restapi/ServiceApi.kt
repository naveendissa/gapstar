package com.cellbrokerage.werkzoeken.restapi

import com.cellbrokerage.werkzoeken.restapi.shift.ShiftResponse
import retrofit2.Call
import retrofit2.http.GET

interface ServiceApi {

    @GET("contractor/shifts")
    fun getShifts(): Call<ShiftResponse>

}