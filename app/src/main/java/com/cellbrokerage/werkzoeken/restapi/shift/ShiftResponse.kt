package com.cellbrokerage.werkzoeken.restapi.shift

import com.cellbrokerage.werkzoeken.model.Job

data class ShiftResponse(val data: Map<String, MutableList<Job>>)

