package com.cellbrokerage.werkzoeken.model

/*
{
        "title": "Horecatoppers voor in de bediening",
        "id": 24809,
        "key": "xvze6p",
        "date": {
          "date": "2019-10-07 14:30:00",
          "timezone_type": 3,
          "timezone": "Europe/Amsterdam"
        },
        "allows_factoring": true,
        "location": {
          "lat": "52.034002",
          "lng": "5.679033"
        },
        "distance": null,
        "url": "https://temper.works/werken-bij/teka-groep-b-v-6/bediening/horecatoppers-voor-in-de-bediening",
        "max_possible_earnings_hour": 15,
        "max_possible_earnings_total": 120,
        "client": {
          "name": "Mauritskazerne // Teka",
          "id": "x7aqry",
          "photos": [
            {
              "formats": [
                {
                  "cdn_url": "https://tmpr-photos.ams3.digitaloceanspaces.com/hero/120443.jpg"
                }
              ]
            }
          ],
          "description": "Een rijksmonument als decor voor een evenement.\r\nDe Mauritskazerne is tussen 1904 en 1906 gebouwd in neo-renaissancestijl. De statige entree, hoge plafonds en grote boogramen geven deze locatie een unieke uitstraling. De industriële look wordt versterkt door het interieur van voornamelijk hergebruikte materialen. De kazerne maakt indruk, je kunt hier in stijl groepen tot 600 gasten ontvangen. In kleinere groepen uit elkaar? Daarvoor hebben we 12 flexibel inzetbare subruimtes beschikbaar.\r\n\r\nDe locatie heeft niet alleen een inspirerend verleden, maar ook een verhaal voor de toekomst. Wat je eet en drinkt komt namelijk van boeren, bakkers en andere foodproducenten uit de regio. Lekker, eerlijk, duurzaam én futureproof.",
          "factoring_allowed": true,
          "factoring_payment_term_in_days": 3,
          "avg_response_time_in_hours": 34
        },
        "job_category": {
          "description": "Serving",
          "icon_path": "/assets/img/web/icons/categories/white-line/icon-bediening.svg",
          "slug": "bediening"
        },
        "open_positions": 3,
        "new_matches_count": 0,
        "photo": "https://tmpr-photos.ams3.digitaloceanspaces.com/hero/118192.jpg",
        "shifts": [
          {
            "id": "pryervp",
            "tempers_needed": 3,
            "earnings_per_hour": 15,
            "duration": 8,
            "currency": "EUR",
            "start_date": "2019-10-07",
            "start_time": "14:30",
            "start_datetime": "2019-10-07 14:30:00",
            "end_time": "23:00",
            "end_datetime": "2019-10-07 23:00:00",
            "is_auto_accepted_when_applied_for": 0
          }
        ]
      }
 */
data class Job(
    val title: String,
    val id: Long,
    val key: String,
    val date: Date,
    val allows_factoring: Boolean,
    val location: Location,
    val distance: String,
    val url: String,
    val max_possible_earnings_hour: Double,
    val max_possible_earnings_total: Double,
    val client: Client,
    val job_category: JobCategory,
    val open_positions: Int,
    val new_matches_count: Int,
    val photo: String,
    val shifts: MutableList<Shift>
)

data class Date(
    val date: String,
    val timezone_type: Int,
    val timezone: String
)

data class Location(
    val lat: Double,
    val lng: Double
)

data class Client(
    val name: String,
    val id: String,
    val photos: MutableList<Photo>,
    val description: String,
    val factoring_allowed: Boolean,
    val factoring_payment_term_in_days: Int,
    val avg_response_time_in_hours: Double
)

data class Photo(val formats: MutableList<Format>)

data class Format(val cdn_url: String)

data class JobCategory(
    val description: String,
    val icon_path: String,
    val slug: String
)

data class Shift(
    val id: String,
    val tempers_needed: Int,
    val earnings_per_hour: Double,
    val duration: Int,
    val currency: String,
    val start_date: String,
    val start_time: String,
    val start_datetime: String,
    val end_time: String,
    val end_datetime: String
)